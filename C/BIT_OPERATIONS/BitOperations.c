// -- http://leetcode.com/2010/09/fun-with-bit-operations.html
// -- QUESTION
/*
    What does the following function mystery() do?
*/

bool mystery(unsigned int x) {
  return x && !(x & (x-1));
}

// -- SOLUTION
/* 
    Assume the bits of x is “101000″, then x-1 is “100111″. The transformation from x » x-1 is easy, 
    the rightmost bit that is ’1′ will be changed to ’0′, and all the ’0′ bits to the right will be 
    changed to ’1′. Therefore, when you do an & operation between them, the rightmost ’1′ bit will turn to ’0′.
    An integer that is a power of two has exactly one bit that is ’1′. 
    Therefore, this function returns whether an integer is a power of two.
*/
