// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Check if the integer is even or odd.
*/
// -- SOLUTION
/*
    The idea here is that an integer is odd if and only if the least significant bit b0 is 1. 
    It follows from the binary representation of 'x', where bit b0 contributes to either 1 or 0. 
    By AND-ing 'x' with 1 we eliminate all the other bits than b0. If the result after this operation is 0, 
    then 'x' was even because bit b0 was 0. Otherwise 'x' was odd.
*/

if ((x & 1) == 0) {
  //x is even
}
else {
  //x is odd
}
