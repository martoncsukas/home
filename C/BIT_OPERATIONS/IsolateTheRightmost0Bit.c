// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Isolate the rightmost 0 bit.
*/
// -- SOLUTION
/*
    This bithack does the opposite of #7. It finds the rightmost 0-bit, turns off all bits, and 
    sets this bit to 1 in the result. For example, it finds the zero in bold in this 
    number 10101011, producing 00000100.
*/

y = ~x & (x+1)
