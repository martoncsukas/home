// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Isolate the rightmost 1-bit.
*/
// -- SOLUTION
/*
    This bit hack finds the rightmost 1-bit and sets all the other bits to 0. 
    The end result has only that one rightmost 1-bit set. For example, 01010100 
    (rightmost bit in bold) gets turned into 00000100.
*/

y = x & (-x)
