// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Test if the n-th bit is set
*/
// -- SOLUTION
/*
    This bit hack improves this result and tests if n-th bit is set. 
    It does it by shifting that first 1-bit n positions to the left and then doing the 
    same AND operation, which eliminates all bits but n-th.
*/

if (x & (1<<n)) {
  //n-th bit is set
}
else {
  //n-th bit is not set
}
