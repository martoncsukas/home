// -- http://leetcode.com/2010/09/number-of-1-bits.html
// -- QUESTION
/*
    Write a function that takes an unsigned integer and returns the number of ’1′ bits it has. 
    For example, the 32-bit integer ’11′ has binary representation 00000000000000000000000000001011,
    so the function should return 3.
*/
// -- SOLUTION
/*
    Remember my last post about making use x & (x-1) to determine if an integer is a power of two? 
    Well, there are even better uses for that! Remember every time you perform the operation x & (x-1), 
    a single 1 bit is erased?
    The following solution is machine independent, and is quite efficient. 
    It runs in the order of the number of 1s. In the worst case, it needs to iterate 32 times 
    (for a 32-bit integer), but a case such as the number ’8′ would only need to iterate 1 time.
*/

int number_of_ones(unsigned int x) {
  int total_ones = 0;
  while (x != 0) {
    x = x & (x-1);
    total_ones++;
  }
  return total_ones;
}
