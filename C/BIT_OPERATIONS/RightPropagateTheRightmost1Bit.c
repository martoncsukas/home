// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Right propagate the rightmost 1-bit.
*/
// -- SOLUTION
/*
    This is best understood by an example. Given a value 01010000 it turns it into 01011111. 
    All the 0-bits right to the rightmost 1-bit got turned into ones.
    This is not a clean hack, tho, as it produces all 1's if x = 0.
*/

y = x | (x-1)
