// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Set the Nth bit
*/
// -- SOLUTION
/*
    This bit hack combines the same (1<<n) trick of setting n-th bit by shifting with OR operation. 
    The result of OR-ing a variable with a value that has n-th bit set is turning that n-th bit on. 
    It's because OR-ing any value with 0 leaves the value the same; but OR-ing it with 1 
    changes it to 1 (if it wasn't already).
*/

y = x | (1<<n)
