// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Toggle the Nth bit.
*/
// -- SOLUTION
/*
    This bit hack also uses the wonderful "set n-th bit shift hack" but this time it XOR's it 
    with the variable 'x'. The result of XOR-ing something with something else is that if both 
    bits are the same, the result is 0, otherwise it's 1. How does it toggle n-th bit? Well, 
    if n-th bit was 1, then XOR-ing it with 1 changes it to 0; conversely, if it was 0, then 
    XOR-ing with with 1 changes it to 1. See, the bit got flipped.
*/

y = x ^ (1<<n)
