// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Turn off the rightmost 1-bit.
*/
// -- SOLUTION
/*
    This bit hack turns off the rightmost one-bit. For example, given an integer 00101010 
    (the rightmost 1-bit in bold) it turns it into 00101000. Or given 00010000 it turns it into 0, 
    as there is just a single 1-bit.
*/

y = x & (x-1)
