// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Turn on the rightmost 0 bit.
*/
// -- SOLUTION
/*
    This hack changes the rightmost 0-bit into 1. 
    For example, given an integer 10100011 it turns it into 10100111.
*/

y = x | (x+1)
