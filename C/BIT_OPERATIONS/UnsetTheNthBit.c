// -- http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/
// -- QUESTION
/*
    Unset the Nth bit
*/
// -- SOLUTION
/*
    The important part of this bithack is the ~(1<<n) trick. It turns on all the bits except n-th.
*/

y = x & ~(1<<n)
