// -- http://leetcode.com/2011/05/a-distance-maximizing-problem.html
// -- QUESTION
/*
    Given an array A of integers, find the maximum of j-i subjected to the constraint of A[i] < A[j].
*/

int maxIndexDiff(int arr[], int n)
{
    int maxDiff;
    int i, j;
 
    int LMin;
    int *RMax = (int *)malloc(sizeof(int)*n);
 
    LMin = arr[0];
 
    RMax[n-1] = arr[n-1];
    for (j = n-2; j >= 0; --j)
        RMax[j] = max(arr[j], RMax[j+1]);
 
     i = 0, j = 0, maxDiff = -1;
    while (j < n && i < n)
    {
        if (LMin < RMax[j])
        {
            maxDiff = max(maxDiff, j-i);
            j = j + 1;
        }
        else
        {
            LMin = min(LMin,arr[i+1]);
            i = i+1;
        }
    }
 
    return maxDiff;
}
