// -- http://leetcode.com/2010/02/here-is-one-of-questions-from-microsoft.html
// -- QUESTION
/*
    Given only putchar (no sprintf, itoa, etc.) write a routine putlong that prints out an unsigned long in decimal.
*/
// -- SOLUTION
/*
    It’s obvious that you can use the modulus operator (%10) and loop thru the digits one-by-one. 
    Since n%10 gives you only the last digit, you need to somehow store it in a temporary array. 
    I am sure the interviewer will not be too pleased with this and ask you to rethink again without using temporary storage.
    The key is to think recursively. 
    Recursion is very powerful and is able to solve this question easily. 
    In fact, it is often used in conjunction with pointers to weed out candidates. 
    Read this article on how Microsoft conduct interviews to get what I mean. 
    Think of recursion as the allocation of a stack… 
    Push the last digit onto the stack, then continue with the 2nd last, …, up until the first digit. 
    Then when the function pops off the stack, you get the digit in the correct order. Voila!
*/

void putLong(unsigned long n) {
  if (n == 0)
    return;
  putLong(n / 10);
  putchar(n % 10 + '0');
}

//special case when n == 0

void putLong(unsigned long n) {
  if (n < 10) {
    putchar(n + '0');
    return;
  }
  putLong(n / 10);
  putchar(n % 10 + '0');
}

