// -- http://leetcode.com/2010/09/detecting-loop-in-singly-linked-list.html
// -- QUESTION
/*
    Given a singly linked list, find if there exist a loop.
*/
// -- SOLUTIOn
/*
    The best solution runs in O(N) time and uses O(1) space. It uses two pointers 
    (one slow pointer and one fast pointer). The slow pointer advances one node at a time, 
    while the fast pointer traverses twice as fast. If the list has loop in it, eventually 
    the fast and slow pointer will meet at the same node. On the other hand, if the loop has 
    no loop, the fast pointer will reach the end of list before the slow pointer does.
*/

bool hasLoop(Node *head) {
  Node *slow = head, *fast = head;
  while (slow && fast && fast->next) {
    slow = slow->next;
    fast = fast->next->next;
    if (slow == fast)
      return true;
  }
  return false;
}

// This elegant algorithm is known as Floyd’s cycle finding algorithm, also called the Tortoise and hare algorithm.
