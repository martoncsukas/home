// -- http://leetcode.com/2010/04/reversing-linked-list-iteratively-and.html
// Implement the reversal of a singly linked list iteratively.

void reverse(Node*& head) {
  if (!head) return;
  Node* prev = NULL;
  Node* curr = head;
  while (curr) {
    Node* next = curr->next;
    curr->next = prev;
    prev = curr;
    curr = next;
  }
  head = prev;
}
