// -- http://leetcode.com/2010/04/reversing-linked-list-iteratively-and.html
// Implement the reversal of a singly linked list recursively.

void reverse(Node*& p) {
  if (!p) return;
  Node* rest = p->next;
  if (!rest) return;
  reverse(rest);
  p->next->next = p;
  p->next = NULL;
  p = rest;
}
