void AlternatingSplit2(struct node* source,
struct node** aRef, struct node** bRef) {
struct node aDummy;
struct node* aTail = &aDummy; // points to the last node in 'a'
struct node bDummy;
struct node* bTail = &bDummy; // points to the last node in 'b'
struct node* current = source;
aDummy.next = NULL;
bDummy.next = NULL;
while (current != NULL) {
MoveNode(&(aTail->next), &current); // add at 'a' tail
aTail = aTail->next; // advance the 'a' tail
if (current != NULL) {
MoveNode(&(bTail->next), &current);
bTail = bTail->next;
}
}
*aRef = aDummy.next;
*bRef = bDummy.next;
}
