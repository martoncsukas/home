void Append(struct node** aRef, struct node** bRef) {
struct node* current;
if (*aRef == NULL) { // Special case if a is empty
*aRef = *bRef;
}
else { // Otherwise, find the end of a, and append b there
current = *aRef;
while (current->next != NULL) { // find the last node
current = current->next;
}24
current->next = *bRef; // hang the b list off the last node
}
*bRef=NULL; // NULL the original b, since it has been appended above
}
