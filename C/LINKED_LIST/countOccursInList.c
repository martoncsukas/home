/*
 Given a list and an int, return the number of times that int occurs
 in the list.
*/

int Count(struct node* head, int searchFor) {
struct node* current = head;
int count = 0;
while (current != NULL) {
if (current->data == searchFor) count++;
current = current->next;
}
return count;
}
