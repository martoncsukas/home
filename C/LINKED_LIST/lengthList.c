// Return the number of nodes in a list
int Length(struct node* head) {
int count = 0;
struct node* current = head;
while (current != NULL) {
count++;
current=current->next;
}
return(count);
}
