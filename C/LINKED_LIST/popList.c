int Pop(struct node** headRef) {
struct node* head;
int result;
head = *headRef;
assert(head != NULL);
result = head->data; // pull out the data before the node is deleted
*headRef = head->next; // unlink the head node for the caller
// Note the * -- uses a reference-pointer
// just like Push() and DeleteList().
free(head); // free the head node
return(result); // don't forget to return the data from the link
}
