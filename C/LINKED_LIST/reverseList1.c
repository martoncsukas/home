/*
This first solution uses the "Push" strategy with the pointer re-arrangement hand coded
inside the loop. There's a slight trickyness in that it needs to save the value of the
"current->next" pointer at the top of the loop since the body of the loop overwrites that
pointer.
*/
/*
 Iterative list reverse.
 Iterate through the list left-right.33
 Move/insert each node to the front of the result list --
 like a Push of the node.
*/
static void Reverse(struct node** headRef) {
struct node* result = NULL;
struct node* current = *headRef;
struct node* next;
while (current != NULL) {
next = current->next; // tricky: note the next node
current->next = result; // move the node onto the result
result = current;
current = next;
}
*headRef = result;
}
