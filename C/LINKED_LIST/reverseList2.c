static void Reverse2(struct node** headRef) {
struct node* result = NULL;
struct node* current = *headRef;
while (current != NULL) {
MoveNode(&result, &current);
}
*headRef = result;
}
