/*
The strategy is to advance up both lists and build the result list as we go. When the
current point in both lists are the same, add a node to the result. Otherwise, advance
whichever list is smaller. By exploiting the fact that both lists are sorted, we only traverse
each list once. To build up the result list, both the dummy node and local reference
strategy solutions are shown...
*/
// This solution uses the temporary dummy to build up the result list
struct node* SortedIntersect(struct node* a, struct node* b) {
struct node dummy;32
struct node* tail = &dummy;
dummy.next = NULL;
// Once one or the other list runs out -- we're done
while (a!=NULL && b!=NULL) {
if (a->data == b->data) {
Push((&tail->next), a->data);
tail = tail->next;
a = a->next;
b = b->next;
}
else if (a->data < b->data) { // advance the smaller list
a = a->next;
}
else {
b = b->next;
}
}
return(dummy.next);
}
