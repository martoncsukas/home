// This solution uses the local reference
struct node* SortedIntersect2(struct node* a, struct node* b) {
struct node* result = NULL;
struct node** lastPtrRef = &result;
// Advance comparing the first nodes in both lists.
// When one or the other list runs out, we're done.
while (a!=NULL && b!=NULL) {
if (a->data == b->data) { // found a node for the intersection
Push(lastPtrRef, a->data);
lastPtrRef = &((*lastPtrRef)->next);
a=a->next;
b=b->next;
}
else if (a->data < b->data) { // advance the smaller list
a=a->next;
}
else {
b=b->next;
}
}
return(result);
}
