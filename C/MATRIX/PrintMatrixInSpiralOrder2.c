// -- http://leetcode.com/2010/05/printing-matrix-in-spiral-order.html
// -- QUESTION
/*
    Given a matrix (2D array) of m x n elements (m rows, n columns), write a function that 
    prints the elements in the array in a spiral manner.
*/
// -- SOLUTION 
/* 
    Iterative
*/

int a[5][6] = { {1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25}
              };
 
int n = 5, m = 5;
int main()
{
    //fii; fio;
    int T, t = 0;
    int rowStrt = 0, rowEnd = n-1;
    int colStrt = 0, colEnd = m-1;
 
    while (rowStrt <= rowEnd) {
        if (rowStrt == rowEnd) {      /// Only 1 row
            for (int j=colStrt; j<=colEnd; j++)
                printf("%d ", a[rowStrt][j]);
            break;
        }
 
        if (colStrt == colEnd) {     /// Only 1 col
            for (int i=rowStrt; i<=rowEnd; i++)
                printf("%d ", a[i][colStrt]);
            break;
        }
 
        for (int j=colStrt; j<colEnd; j++)
            printf("%d ", a[rowStrt][j]);
        for (int i=rowStrt; icolStrt; j--)
            printf("%d ", a[rowEnd][j]);
        for (int i=rowEnd; i>rowStrt; i--)
            printf("%d ", a[i][colStrt]);
 
        colStrt++, colEnd--;
        rowStrt++, rowEnd--;
        printf("\nrowStrt = %d, rowEnd = %d\n", rowStrt, rowEnd);
    }
 
    fprintf(stderr, "Time execute: %.3lf\n", clock() / (double)CLOCKS_PER_SEC);
    return 0;
}
