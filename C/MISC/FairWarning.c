// -- http://leetcode.com/2010/05/problem-b-fair-warning-solution.html
// -- https://code.google.com/codejam/contest/dashboard?c=433101#s=p1

long long gcd(long long a, long long b) {
  if (b == 0)
    return a;
  return gcd(b, a % b);
}
 
long long gcd(long long arr[], int n) {
  long long temp = arr[0];
  for (int i = 1; i < n; i++) {
    temp = gcd(temp, arr[i]);
  }
  return temp;
}
 
int n, m;
long long arr[1400], arr2[100000];
  cin >> n;
for (int i = 0; i < n; i++) {
  cin >> m;
  for (int j = 0; j < m; j++) 
    cin >> arr[j];
 
  sort(arr, arr+m);
  int sz = 0;
  for (int j = m-1; j >=0; j--) {
    for (int k = j-1; k >= 0; k--) {
      arr2[sz++] = arr[j]-arr[k];
    }
  }
  long long T = gcd(arr2, sz);
  long long ans = (ceil((double)arr[0] / T))*T - arr[0];
  cout << "Case #" << i+1 << ": " << ans << endl;
}
