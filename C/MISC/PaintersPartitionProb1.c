// -- http://leetcode.com/2011/04/the-painters-partition-problem.html
// -- QUESTION
/*
    You have to paint N boards of length {A0, A1, A2 … AN-1}. 
    There are K painters available and you are also given how much time a painter 
    takes to paint 1 unit of board. You have to get this job done as soon as possible 
    under the constraints that any painter will only paint continuous sections of 
    board, say board {2, 3, 4} or only board {1} or nothing but not board {2, 4, 5}.
*/

int sum(int A[], int from, int to) {
  int total = 0;
  for (int i = from; i <= to; i++)
    total += A[i];
  return total;
}
 
int partition(int A[], int n, int k) {
  if (k == 1)
    return sum(A, 0, n-1);
  if (n == 1)
    return A[0];
 
  int best = INT_MAX;
  for (int j = 1; j <= n; j++)
    best = min(best, max(partition(A, j, k-1), sum(A, j, n-1)));
 
  return best;
}
