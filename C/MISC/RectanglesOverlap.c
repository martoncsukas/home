// -- http://leetcode.com/2011/05/determine-if-two-rectangles-overlap.html
// -- QUESTION
/*
    Given two axis-aligned rectangles A and B. 
    Write a function to determine if the two rectangles overlap.
*/

//The condition’s expression is:
! ( P2.y < P3.y || P1.y > P4.y || P2.x < P3.x || P1.x > P4.x )

//Using De Morgan’s law, we can further simplify the above expression to:
( P2.y = P3.y && P1.y = P4.y && P2.x = P3.x && P1.x = P4.x )
