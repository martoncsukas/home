// -- http://leetcode.com/2010/05/problem-snapper-chain-gcj-qualification.html
// -- https://code.google.com/codejam/contest/dashboard?c=433101#s=p0


int T, n, k;
    cin >> T;
    for (int i = 0; i < T; i++) {
    cin >> n >> k;
    cout << "Case #" << i + 1 << ": ";
    if (((1 << (n-1)) ^ (k & ((1 << n)-1))) == ((1 << (n-1))-1)) {
      cout << "ON\n";
    } else {
      cout << "OFF\n";
    }
}
