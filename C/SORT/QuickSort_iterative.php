// QUICKSORT    - http://en.wikipedia.org/wiki/Quicksort
// Exchange sort (comparsion sort)
// O(n log(n))  - Best Time Complexity
// O(n log(n))  - Average Time Complexity
// O(n^2)       - Worst Time Complexity
// O(n)         - Worst Case Auxiliary Space Complexity
// -------------------------------------------------------

$list = array(5,3,9,8,7,2,4,1,6,5);
 
// iterative
function quicksort_iterative($array)
{
    $stack = array($array);
    $sorted = array();
 
    while (count($stack) > 0) {
 
        $temp = array_pop($stack);
 
        if (count($temp) == 1) {
            $sorted[] = $temp[0];
            continue;
        }
 
        $pivot = $temp[0];
        $left = $right = array();
 
        for ($i = 1; $i < count($temp); $i++) {
            if ($pivot > $temp[$i]) {
                $left[] = $temp[$i];
            } else {
                $right[] = $temp[$i];
            }
        }
 
        $left[] = $pivot;
 
        if (count($right))
            array_push($stack, $right);
        if (count($left))
            array_push($stack, $left);
    }
 
    return $sorted;
}
 
// 1, 2, 3, 4, 5, 5, 6, 7, 8, 9
print_r(quicksort_iterative($list));
