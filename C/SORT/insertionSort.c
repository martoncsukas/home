// INSERTION SORT  - http://en.wikipedia.org/wiki/Insertion_sort
// Insertion sort
// O(n)         - Best Time Complexity
// O(n^2)       - Average Time Complexity
// O(n^2)       - Worst Time Complexity
// O(1)         - Worst Case Auxiliary Space Complexity
// -------------------------------------------------------

#include <stdio.h>
 
int main()
{
  int n, array[1000], c, d, t;
 
  printf("Enter number of elements\n");
  scanf("%d", &n);
 
  printf("Enter %d integers\n", n);
 
  for (c = 0; c < n; c++) {
    scanf("%d", &array[c]);
  }
 
  for (c = 1 ; c <= n - 1; c++) {
    d = c;
 
    while ( d > 0 && array[d] < array[d-1]) {
      t          = array[d];
      array[d]   = array[d-1];
      array[d-1] = t;
 
      d--;
    }
  }
 
  printf("Sorted list in ascending order:\n");
 
  for (c = 0; c <= n - 1; c++) {
    printf("%d\n", array[c]);
  }
 
  return 0;
}
