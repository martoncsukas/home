// -- http://leetcode.com/2010/02/c-code-to-remove-spaces-from-string.html
// -- QUESTION
/* 
    Write a C function to remove spaces from a string. The function header should be 
    void removeSpacesFromString2(char *str)
    “abc de” -> “abcde” 
*/
// -- SOLUTION
/* 
    There are no parenthesis indicating clear scope and it’s not obvious what *p1++ means 
    to some programmers (*p1++ is the same as *(p1++) due to ++ having higher precedence).
*/

void removeSpacesFromString2(char *str) {
  char *p1 = str, *p2 = str;
  do 
    while (*p2 == ' ')
      p2++;
  while (*p1++ = *p2++);
}
