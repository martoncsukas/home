/*
Print a string in reverse order.
*/

void printStringReversed(const char *str) {
  if (!*str)
    return;
  printReverse(str + 1);
  putchar(*str);
}
