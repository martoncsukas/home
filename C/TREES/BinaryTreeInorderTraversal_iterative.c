// -- http://leetcode.com/2010/04/binary-search-tree-in-order-traversal.html
// -- QUESTION
/*
    Given a binary search tree, print the elements in-order iteratively without using recursion.
    We need a stack to store previous nodes, and a visited flag for each node is needed to record 
    if the node has been visited before. When a node is traversed for the second time, its value 
    will be printed. After its value is printed, we push its right child and continue from there.
*/

void in_order_traversal_iterative(BinaryTree *root) {
  stack<BinaryTree*> s;
  s.push(root);
  while (!s.empty()) {
    BinaryTree *top = s.top();
    if (top != NULL) {
      if (!top->visited) {
        s.push(top->left);
      } else {
        cout << top->data << " ";
        s.pop();
        s.push(top->right);
      }
    } else {
      s.pop();
      if (!s.empty())
        s.top()->visited = true;
    }
  }
}
