// -- http://leetcode.com/2010/04/binary-search-tree-in-order-traversal.html
// -- QUESTION
/*
    Given a binary search tree, print the elements in-order iteratively with recursion.
*/

void in_order_traversal(BinaryTree *p) {
  if (!p) return;
  in_order_traversal(p->left);
  cout << p->data;
  in_order_traversal(p->right);
}
