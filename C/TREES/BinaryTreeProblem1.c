// -- http://leetcode.com/2010/03/first-on-site-technical-interview.html
// -- QUESTION
/*
    Given a binary tree

    struct Node {
      Node* leftChild;
      Node* rightChild;
      Node* nextRight;
    }

    Populate the nextRight pointers in each node.

    You may assume that it is a full binary tree (ie, each node other than the leaves has two children.)
*/
// -- SOLUTION
/*
    Most likely this can be implemented recursively, because you can identify the linking of nodes as sub-problems.
    The main difficulty of this problem is linking rightChild with the nextSibling of rightChild.
    Each node has no parent pointer. Therefore, there is no way linking the rightChild with its nextSibling at a level.
*/

void connect(Node* p) {
  if (p == NULL)
    return;
  if (p->leftChild == NULL || p->rightChild == NULL)
    return;
  Node* rightSibling;
  Node* p1 = p;
  while (p1) {
    if (p1->nextRight)
      rightSibling = p1->nextRight->leftChild;
    else
      rightSibling = NULL;
    p1->leftChild->nextRight = p1->rightChild;
    p1->rightChild->nextRight = rightSibling;
    p1 = p1->nextRight;
  }
  connect(p->leftChild);
}
