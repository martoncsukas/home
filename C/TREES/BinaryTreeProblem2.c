// -- http://leetcode.com/2010/03/first-on-site-technical-interview.html
// -- QUESTION
/*
    Given a binary tree

    struct Node {
      Node* leftChild;
      Node* rightChild;
      Node* nextRight;
    }

    Populate the nextRight pointers in each node.

    You may assume that it is a full binary tree (ie, each node other than the leaves has two children.)
*/
// -- SOLUTION
/*
    Here is a more elegant solution. The trick is to re-use the populated nextRight pointers. 
    As mentioned earlier, we just need one more step for it to work. Before we passed the leftChild 
    and rightChild to the recursion function itself, we connect the rightChild’s nextRight to the 
    current node’s nextRight’s leftChild. In order for this to work, the current node’s nextRight 
    pointer must be populated, which is true in this case. Why? Try to draw a series of diagram how the recursion deepens,     you will immediately see that it is doing DFS (Depth first search).
*/

void connect(Node* p) {
  if (!p) return;
  if (p->leftChild)
  p->leftChild->nextRight = p->rightChild;
  if (p->rightChild)
    p->rightChild->nextRight = (p->nextRight) ?
                               p->nextRight->leftChild :
                               NULL;
  connect(p->leftChild);
  connect(p->rightChild);
}
