//---------------------------------------------------------------------------------------------------------------
// -- http://leetcode.com/2010/04/finding-all-unique-triplets-that-sums.html
// -- http://oj.leetcode.com/problems/3sum/
//---------------------------------------------------------------------------------------------------------------
// -- QUESTION
/*
      Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? 
      Find all unique triplets in the array which gives the sum of zero.
      Note:
      Elements in a triplet (a,b,c) must be in non-descending order. (ie, a ≤ b ≤ c)
      The solution set must not contain duplicate triplets.
          For example, given array S = {-1 0 1 2 -1 -4},
      
          A solution set is:
          (-1, 0, 1)
          (-1, -1, 2)
*/
//---------------------------------------------------------------------------------------------------------------
// -- SOLUTION
/* 
      judge samll:passed
      judge large:Time limit exceeded
      time complexity: O(n^3)
      Memo:        java.util.Arrays to use Arrays.sort(); 1)It is in place;
	           java.util.ArrayList to use ArrayList; 1)ArrayList.get(index) to get 
	           one element;2)ArrayList.contains()
*/
//---------------------------------------------------------------------------------------------------------------

import java.util.Arrays;
import java.util.ArrayList;
public class Solution {
    public ArrayList<ArrayList<Integer>> threeSum(int[] num) {

        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        
        if(num.length<3)
            return result;
        
        Arrays.sort(num);        
        for(int i=0;i<num.length-2;i++)
            for(int j=i+1;j<num.length-1;j++)
                for(int p=j+1;p<num.length;p++){
                    if(num[i]+num[j]+num[p]==0){
                         ArrayList<Integer> triplet = new ArrayList<Integer>(3);
                         triplet.add(num[i]);
                         triplet.add(num[j]);
                         triplet.add(num[p]);
                         if(!result.contains(triplet))
                            result.add(triplet);
                    }
                }
        
        return result;
    }
        
}
