//---------------------------------------------------------------------------------------------------------------
// -- http://leetcode.com/2010/04/finding-all-unique-triplets-that-sums.html
// -- http://oj.leetcode.com/problems/3sum/
//---------------------------------------------------------------------------------------------------------------
// -- QUESTION
/*
      Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? 
      Find all unique triplets in the array which gives the sum of zero.
      Note:
      Elements in a triplet (a,b,c) must be in non-descending order. (ie, a ≤ b ≤ c)
      The solution set must not contain duplicate triplets.
          For example, given array S = {-1 0 1 2 -1 -4},
      
          A solution set is:
          (-1, 0, 1)
          (-1, -1, 2)
*/
//---------------------------------------------------------------------------------------------------------------
// -- SOLUTION
/* 
    judge samll:passed
    judge large:Time limit exceeded
    time complexity: O(n^2logn)
*/
//---------------------------------------------------------------------------------------------------------------

import java.util.Arrays;
import java.util.ArrayList;
public class Solution {
    public ArrayList<ArrayList<Integer>> threeSum(int[] num) {

        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        
        if(num.length<3)
            return result;
        
        Arrays.sort(num);        
        for(int i=0;i<num.length-2;i++)                  
            for(int j=i+1;j<num.length-1;j++)
                if(binarySearch(num,j+1,num.length-1,-num[i]-num[j])>=0){
                    ArrayList<Integer> triplet = new ArrayList<Integer>(3);
                         triplet.add(num[i]);
                         triplet.add(num[j]);
                         triplet.add(-num[i]-num[j]);
                         if(!result.contains(triplet))
                            result.add(triplet);
                }          
        return result;
    }
    
    public static int binarySearch(int[] num,int p,int r, int val){
        if(num==null || num.length==0 || p>r)
            return -1;
        int m = (p+r)/2;
        if( val ==num[m])
            return m;
        else if (val>num[m])
            return binarySearch(num,m+1,r,val);
        else
            return binarySearch(num,p,m-1,val);
    }
        
}
