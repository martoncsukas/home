//---------------------------------------------------------------------------------------------------------------
// -- http://leetcode.com/2010/04/finding-all-unique-triplets-that-sums.html
// -- http://oj.leetcode.com/problems/3sum/
//---------------------------------------------------------------------------------------------------------------
// -- QUESTION
/*
      Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.
      Note:
      Elements in a triplet (a,b,c) must be in non-descending order. (ie, a ≤ b ≤ c)
      The solution set must not contain duplicate triplets.
          For example, given array S = {-1 0 1 2 -1 -4},
      
          A solution set is:
          (-1, 0, 1)
          (-1, -1, 2)
*/
//---------------------------------------------------------------------------------------------------------------
// -- SOLUTION
/* 
    judge samll:passed
    judge large: passed mostly，around 700-800 ms
    time complexity: O(n^2)
    Based on 3Sum3.java, applied the avoid-redundency method by Chi Su.
*/
//---------------------------------------------------------------------------------------------------------------

import java.util.*;

public class Solution {
    public int increment(int[] num, int index) {
        int newindex = index + 1;
        while(newindex < num.length && num[newindex] == num[index])
            newindex ++;
        return newindex;
    }

    public int decrement(int[] num, int index) {
        int newindex = index - 1;
        while(newindex >= 0  && num[newindex] == num[index])
            newindex --;
        return newindex;
    }
    public ArrayList<ArrayList<Integer>> threeSum(int[] num) {

        ArrayList<ArrayList<Integer>> ret = new ArrayList<ArrayList<Integer>>();
        if (num.length < 3)
            return ret;

        Arrays.sort(num);
        for (int i = 0; i < num.length - 2; i = increment(num, i)) {
            int p = i + 1, q = num.length - 1;
            while (p < q) {
                if (num[p] + num[q] == -num[i]) {
                    ArrayList<Integer> triplet = new ArrayList<Integer>(3);
                    triplet.add(num[i]);
                    triplet.add(num[p]);
                    triplet.add(num[q]);
                    
                    ret.add(triplet);
                    p = increment(num, p);
                    q = decrement(num, q);
                } else if (num[p] + num[q] > -num[i])
                    q = decrement(num, q);
                else
                    p = increment(num, p);
            }
        }

        return ret;

    }



}
