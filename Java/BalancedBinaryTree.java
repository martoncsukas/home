//---------------------------------------------------------------------------------------------------------------
// -- http://oj.leetcode.com/problems/balanced-binary-tree/
//---------------------------------------------------------------------------------------------------------------
// -- QUESTION
/*
    Given a binary tree, determine if it is height-balanced.
    For this problem, a height-balanced binary tree is defined as a binary tree in which 
    the depth of the two subtrees of every node never differ by more than 1.
*/
//---------------------------------------------------------------------------------------------------------------
// -- SOLUTION
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
 //---------------------------------------------------------------------------------------------------------------
 
public class Solution {
    public boolean isBalanced(TreeNode root) {

        if(root == null) return true;
        int leftSubtreeHeight = height(root.left);
        int rightSubtreeHeight = height(root.right);
        return Math.abs(leftSubtreeHeight-rightSubtreeHeight)<=1 &&
            isBalanced(root.left) && isBalanced(root.right);    
    }
       
    public int height(TreeNode node){
        if(node ==null) return 0;
        
        return 1+Math.max(height(node.left),height(node.right));
    }
    
    
}
