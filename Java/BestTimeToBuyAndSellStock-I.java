//---------------------------------------------------------------------------------------------------------------
// -- http://oj.leetcode.com/discuss/oj/best-time-to-buy-and-sell-stock
//---------------------------------------------------------------------------------------------------------------
// -- QUESTION
/*
    Say you have an array for which the i^th element is the price of a given stock on day i.
    If you were only permitted to complete at most one transaction (ie, buy one and sell 
    one share of the stock), design an algorithm to find the maximum profit.
*/
//---------------------------------------------------------------------------------------------------------------

public class Solution {
    public int maxProfit(int[] prices) {
        // Start typing your Java solution below
        // DO NOT write main() function
        if(prices==null || prices.length==0)
            return 0;

        int bot = prices[0];
        int maxProfit = 0;
        for(int i=0;i<prices.length;i++){
                if(prices[i]<bot)
                        bot = prices[i];
                else if(prices[i]-bot>maxProfit)
                        maxProfit = prices[i]-bot;
        }

        return maxProfit;

    }
}
