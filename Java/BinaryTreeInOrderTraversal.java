//---------------------------------------------------------------------------------------------------------------
// -- http://oj.leetcode.com/problems/binary-tree-inorder-traversal/
//---------------------------------------------------------------------------------------------------------------
// -- QUESTION
/*
    Given a binary tree, return the inorder traversal of its nodes' values.
    For example:
    Given binary tree {1,#,2,3},
       1
        \
         2
        /
       3
    return [1,3,2].
    
    Note: Recursive solution is trivial, could you do it iteratively?
*/
//---------------------------------------------------------------------------------------------------------------
// -- SOLUTION
/*
    Iterative Binary Tree Inorder Traversal.
    For Preoder and Postorder, please refer to 
    http://www.brilliantsheep.com/iterative-binary-tree-traversal-in-java/
*/
/*
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
//---------------------------------------------------------------------------------------------------------------

import java.util.*;
public class Solution {
    public ArrayList<Integer> inorderTraversal(TreeNode root) {

        ArrayList<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode node= root;
        
        while(!stack.isEmpty() || node!=null){
            if(node != null){
                stack.push(node);
                node = node.left;
            }else {
                node = stack.pop();
                result.add(node.val);            
                node = node.right;
            }
        }
        return result;
        
    }
}
