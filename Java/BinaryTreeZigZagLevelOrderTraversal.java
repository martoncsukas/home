// -- http://oj.leetcode.com/problems/binary-tree-zigzag-level-order-traversal/
// -- QUESTION
/*
    Given a binary tree, return the zigzag level order traversal of its nodes' values. 
    (ie, from left to right, then right to left for the next level and alternate between).
    For example:
    Given binary tree {3,9,20,#,#,15,7},
        3
       / \
      9  20
        /  \
       15   7
    return its zigzag level order traversal as:
    [
      [3],
      [20,9],
      [15,7]
    ]
*/
// -- SOLUTION
/*
    It's similar to binaryTreeLevelOrderTraversal except we use stack and do some 
    minor change. 
/*
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

public class Solution {
    public ArrayList<ArrayList<Integer>> zigzagLevelOrder(TreeNode root) {
        // Start typing your Java solution below
        // DO NOT write main() function
        ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
        if(root==null)  return res;
        Stack<TreeNode> q = new Stack<TreeNode>();
        q.push(root);
        boolean leftToRight = true;
            
        while(!q.empty()){
            Stack<TreeNode> p = new Stack<TreeNode>();
            ArrayList<Integer> clvl = new ArrayList<Integer>();
            
            while(!q.empty()){
                TreeNode node = q.pop();
                clvl.add(node.val);
                if(leftToRight){
                    if(node.left!=null)
                        p.push(node.left);
                    if(node.right!=null)
                        p.push(node.right); }  
                else{
                    if(node.right!=null)
                        p.push(node.right);
                     if(node.left!=null)
                        p.push(node.left); }                        
            }
            leftToRight=!leftToRight;
            res.add(clvl);
            q = p;

        }
        
        return res;
    }
}
