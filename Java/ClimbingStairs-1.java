// -- http://oj.leetcode.com/problems/climbing-stairs/
// -- QUESTION
/*
    You are climbing a stair case. It takes n steps to reach to the top.
    Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
*/
// -- SOLUTION
/*
    judge small : passed
    judge large : time limit exceeded
*/

public class Solution {
    public int climbStairs(int n) {
        // Start typing your Java solution below
        // DO NOT write main() function
        if(n==0)
            return 0;
        else
            return climbStairs(0,n);
        
    }
    
    public int climbStairs(int level,int n){
        if(level> n)
            return 0;
        else if(level==n)
            return 1;
        else 
            return climbStairs(level+1,n)+climbStairs(level+2,n);
        
    }
}
