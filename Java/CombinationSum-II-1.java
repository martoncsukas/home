// -- http://oj.leetcode.com/problems/combination-sum-ii/
// -- QUESTION
/*
    Given a collection of candidate numbers (C) and a target number (T), find all unique combinations in C where the candidate numbers sums to T.
    Each number in C may only be used once in the combination.
    Note:
    All numbers (including target) will be positive integers.
    Elements in a combination (a1, a2, … , ak) must be in non-descending order. (ie, a1 ≤ a2 ≤ … ≤ ak).
    The solution set must not contain duplicate combinations.
    For example, given candidate set 10,1,2,7,6,1,5 and target 8, 
    A solution set is: 
    [1, 7] 
    [1, 2, 5] 
    [2, 6] 
    [1, 1, 6] 
*/
// -- SOLUTION
/*
    judge small :passed 560ms
    judge large :time limit exceeded sometimes 950ms

    This is a bad solution in terms of performance.

*/

import java.util.*;
public class Solution {
    public ArrayList<ArrayList<Integer>> combinationSum2(int[] num, int target) {
        // Start typing your Java solution below
        // DO NOT write main() function
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        Arrays.sort(num);
        combinationSum2(num,target,0,new ArrayList<Integer>(),result);
        return result;
    }
    
    public void combinationSum2(int[] num, int target, int pointer, ArrayList<Integer> currentCombo,ArrayList<ArrayList<Integer>> result) {
        if(target==0){
            if(!result.contains(currentCombo))
                result.add(new ArrayList<Integer>(currentCombo));
            return;
        }else if(target<0 || pointer<0)
            return;
        
        for(int i=pointer;i<num.length;i++){
            currentCombo.add(num[i]);
            combinationSum2(num,target-num[i],i+1,currentCombo,result);
            currentCombo.remove(currentCombo.size()-1);
        }
    
    }
}
