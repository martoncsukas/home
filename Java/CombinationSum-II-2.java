// -- http://oj.leetcode.com/problems/combination-sum-ii/
// -- QUESTION
/*
    Given a collection of candidate numbers (C) and a target number (T), find all unique combinations in C where the candidate numbers sums to T.
    Each number in C may only be used once in the combination.
    Note:
    All numbers (including target) will be positive integers.
    Elements in a combination (a1, a2, … , ak) must be in non-descending order. (ie, a1 ≤ a2 ≤ … ≤ ak).
    The solution set must not contain duplicate combinations.
    For example, given candidate set 10,1,2,7,6,1,5 and target 8, 
    A solution set is: 
    [1, 7] 
    [1, 2, 5] 
    [2, 6] 
    [1, 1, 6] 
*/
// -- SOLUTION
/*
    judge small :passed 510ms
    judge large :passed 670ms 

*/


    
import java.util.*;
public class Solution {
    public ArrayList<ArrayList<Integer>> combinationSum2(int[] num, int target) {
        // Start typing your Java solution below
        // DO NOT write main() function    
        Arrays.sort(num);
        return find_combinationSum2(num,target,num.length-1);
    }
    
    public ArrayList<ArrayList<Integer>> find_combinationSum2(int[] num, int target, int pointer){
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        if(target==0)
            result.add(new ArrayList<Integer>());
        else if(target>0)  
            for(int i=pointer;i>=0;i--)
                if(i+1<=pointer  && num[i]==num[i+1])  // a better way to avoid redundancy than "if(!result.contains(combo))"
                    continue;
                else
                    for(ArrayList<Integer> combo:find_combinationSum2(num,target-num[i],i-1)){
                        combo.add(num[i]);
                        // if(!result.contains(combo))
                        result.add(combo);
                    }     
        return result;
    
    }
}
