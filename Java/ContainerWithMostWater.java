// -- http://oj.leetcode.com/problems/container-with-most-water/
// -- QUESTION
/*
    Given n non-negative integers a1, a2, ..., an, where each represents a point 
    at coordinate (i, ai). n vertical lines are drawn such that the two endpoints 
    of line i is at (i, ai) and (i, 0). Find two lines, which together with x-axis
    forms a container, such that the container contains the most water.
    Note: You may not slant the container.
*/

public class Solution {
    public int maxArea(int[] height) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int max = 0 , left=0,right= height.length-1;
        while(left<right){
            int d = Math.min(height[left],height[right]);
            int area = d*(right-left);
            if(area>max)
                max = area;
            
            if(d == height[left])
                while(left<height.length && height[left]<=d)
                    left++;            
            else
                while(right>=0 && height[right]<=d)
                    right--;
        }  
        return max;
    }
}
