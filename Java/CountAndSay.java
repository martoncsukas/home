// -- http://oj.leetcode.com/problems/count-and-say/
// -- QUESTION
/*
    The count-and-say sequence is the sequence of integers beginning as follows:
    1, 11, 21, 1211, 111221, ...
    
    1 is read off as "one 1" or 11.
    11 is read off as "two 1s" or 21.
    21 is read off as "one 2, then one 1" or 1211.
    Given an integer n, generate the nth sequence.
    
    Note: The sequence of integers will be represented as a string.
*/
// -- SOLUTION
/*
    Although this is a very easy problem, it takes a while before me figuring out the exact right algorithm.
*/

public class Solution {
    public String countAndSay(int n) {
        // Start typing your Java solution below
        // DO NOT write main() function
        String current="1", next="";
        if(n==1)
            return current;
        
        for(int i=2;i<=n;i++){
            int count=1;
            for(int j=0;j<current.length();j++){
                if(j>0) 
                    if(current.charAt(j)==current.charAt(j-1))
                        count++;
                    else{
                        next=next+count+current.charAt(j-1);
                        count=1;
                    }          
                if(j == current.length()-1)
                    next=next+count+current.charAt(j);
                
                                    
            }
            current = next;
            next = "";
        }
        
        return current;
        
    }
}
