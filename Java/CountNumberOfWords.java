// -- http://leetcode.com/2010/02/c-code-to-count-number-of-words-in.html
// -- QUESTION
/*
    Count the number of words in a string, where a word is defined to be a contiguous 
    sequence of non-space characters.
    eg, “Hello, my name is John.” -> 5
*/

public class countNumberOfWords2 {
 
    @Test
    public void test() {
        count("Hello, my name is John");
        count(" Hello, my name is John");
        count("Hello,   my   name is John  ");
        count("Hello   ");
        count("Hello ");
        count("   Hello   ");
        count("Hello   ");
        count("Hello ");
        count("H     ");
        count(" H ");
        count(null);
    }
 
    private void count(String source) {
        int count = 0;
        if (source == null) {
            System.out.println("Count: " + count);
            return;
        }
        boolean isBlank = true;
        for (int i = 0; i < source.length(); i++) {
            if (source.charAt(i) == ' ' || source.charAt(i) == '\t') {
                isBlank = true;
            } else {
                if (isBlank == true) {
                    isBlank = false;
                    count++;
                }
            }
        }
 
        System.out.println("Count: " + count + ", Source: " + source);
    }
}
