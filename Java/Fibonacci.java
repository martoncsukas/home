// Write a function that computes the Nth Fibonacci number

static long fib(int n) {
        return n <= 1 ? n : fib(n-1) + fib(n-2);
}


// Test harness
public static void main ( String[] args ) {
        for ( int i = 0; i < 10; i++ ) {
            System.out.print ( fib(i) + ", " );
        }
        System.out.println ( fib(10) );
}
