// -- http://leetcode.com/2010/03/here-is-phone-screening-question-from.html
// -- QUESTION
/*
    Intersect 2 sorted int arrays of sizes m & n
    can contain duplicates
    A=[1,4,6,8,15] 
    B=[4,6,9,10,15,21,22,24] Intersection = [4,6,9]
*/
public List IntersectArrays(int[] array1, int[] array2)
{
    int array1_length = array1.Length; // length m
    int array2_length = array2.Length; // length n 
 
    if((array1_length  == 0) || (array2_Length==0))
    {
        return null;
    }
    
    int index1 =0;
    int index2 =0;
    
    List intersectedArray = new List();
    
    // Complexity O(m+n)
    while((index1<=array1_length) && index2<=array2_length) //if loop condition fails all intersections already found (array 1 is bigger)
    {   
           if(array1[index1]  array2[index2])
           {
               index2++;
           }
           else // found an intersection.
           {
              interetedArray.Add(array1[index1]);
              index1++;
              index2++;
           }
    }
    
    return intersectedArray;
}
