// -- http://oj.leetcode.com/problems/flatten-binary-tree-to-linked-list/
// -- QUESTION
/*
    Given a binary tree, flatten it to a linked list in-place. For example, given

         1
        / \
       2   5
      / \   \
     3   4   6
    
    The flattened tree should look like:
   
    1
     \
      2
       \
        3
         \
          4
           \
            5
             \
              6

    If you notice carefully in the flattened tree, each node's right child points to the 
    next node of a pre-order traversal.
*/
// -- SOLUTION
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
 
public class Solution {
    public void flatten(TreeNode root) {
        // Start typing your Java solution below
        // DO NOT write main() function
        if(root==null)
            return;
        
        TreeNode left  = root.left;
        TreeNode right = root.right;
        root.left = null;
        if(left != null){
            root.right = left;                
            TreeNode rightmost = left;
            while(rightmost.right!=null)
                rightmost = rightmost.right;
            rightmost.right =  right;
        }else
            root.right = right;
       
        flatten(left);
        flatten(right);
        
    }
}
