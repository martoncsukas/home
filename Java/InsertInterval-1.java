// -- http://oj.leetcode.com/problems/insert-interval/
// -- QUESTION
/*
    Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).
    
    You may assume that the intervals were initially sorted according to their start times.
    
    Example 1:
    Given intervals [1,3],[6,9], insert and merge [2,5] in as [1,5],[6,9].
    
    Example 2:
    Given [1,2],[3,5],[6,7],[8,10],[12,16], insert and merge [4,9] in as [1,2],[3,10],[12,16].
    
    This is because the new interval [4,9] overlaps with [3,5],[6,7],[8,10].
*/
// -- SOLUTION
/*
        judge small :540ms
        judge large::650ms

        This interview question is tougher than it appears. I tend to make a lot of 
        mistakes when coding it.
*/
/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
 
public class Solution {
    public ArrayList<Interval> insert(ArrayList<Interval> intervals, Interval newInterval) {
        // Start typing your Java solution below
        // DO NOT write main() function
        ArrayList<Interval> res = new ArrayList<Interval>();        
        Interval pre= new Interval(newInterval.start,newInterval.end);
        
        boolean inserted = false;
        int i=0;
        while(i<intervals.size() || !inserted){
            if(i<intervals.size()){
                Interval current= intervals.get(i);
                if(inserted)
                    res.add(current);
                else if(current.end<pre.start)
                    res.add(current);
                else if(isOverlap(current,pre)){
                    pre.start = Math.min(current.start,pre.start);
                    pre.end = Math.max(current.end,pre.end);
                }
                 else{
                     res.add(pre);
                     res.add(current);
                     inserted=true;
                 }
            }else{
                res.add(pre);
                inserted=true;
            }
                
            i++;
                
        }     
        return res;


    }
    public boolean isOverlap(Interval a, Interval b){
            return !(b.start>a.end || a.start> b.end);
    }
}        
