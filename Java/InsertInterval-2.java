// -- http://oj.leetcode.com/problems/insert-interval/
// -- QUESTION
/*
    Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).

    You may assume that the intervals were initially sorted according to their start times.
    
    Example 1:
    Given intervals [1,3],[6,9], insert and merge [2,5] in as [1,5],[6,9].
    
    Example 2:
    Given [1,2],[3,5],[6,7],[8,10],[12,16], insert and merge [4,9] in as [1,2],[3,10],[12,16].
    
    This is because the new interval [4,9] overlaps with [3,5],[6,7],[8,10].
*/
// -- SOLUTION
/*
    judge small :540ms
    judge large::650ms

    This is simplified version of insertinterval1.java : When we are checking each interval i in intervals, once we find i should come after the inserting interval t, we append t, and then go ahead and append the rest of the elements in intervals to the result list and return it.

    Another way is to use segment tree（http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=lowestCommonAncestor#Segment_Trees）.
*/

/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
 
import java.util.*;
public class Solution {
    public ArrayList<Interval> insert(ArrayList<Interval> intervals, Interval newInterval) {
        // Start typing your Java solution below
        // DO NOT write main() function
        ArrayList<Interval> res = new ArrayList<Interval>();        
        Interval t= new Interval(newInterval.start,newInterval.end);
        Iterator<Interval> itr = intervals.iterator();
        
        while(itr.hasNext()){
            Interval i = itr.next();
            if(i.start>t.end){
                res.add(t);
                res.add(i);
                while(itr.hasNext()){res.add(itr.next());}
                return res;
            }
            
            if(t.start>i.end) 
                res.add(i);
            else{
                 t.start = Math.min(i.start,t.start);
                 t.end = Math.max(i.end,t.end);   
            }
        }
        res.add(t);
        return res;

    }
}     
