// -- http://oj.leetcode.com/problems/jump-game/
// -- QUESTION
/*
    Given an array of non-negative integers, you are initially positioned at the first index of the array.

    Each element in the array represents your maximum jump length at that position.
    
    Determine if you are able to reach the last index.
    
    For example:
    A = [2,3,1,1,4], return true.
    
    A = [3,2,1,0,4], return false.
*/
// -- SOLUTION
/*
    judge small: passed 510ms
    judge large: passed 540ms
    Inspired by Su Shao. 
*/

public class Solution {
    public boolean canJump(int[] A) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int i=0;
        int range= 0;
        
        while(i<=range){
            range = Math.max(range,i+A[i]);
            if(range>=A.length-1)
                return true;
            i++;
        }
        return false;
        
    }
}
