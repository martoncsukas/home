// -- http://oj.leetcode.com/problems/jump-game-ii/
// -- QUESTION
/*
    Given an array of non-negative integers, you are initially positioned at the first index of the array.

    Each element in the array represents your maximum jump length at that position.
    
    Your goal is to reach the last index in the minimum number of jumps.
    
    For example:
    Given array A = [2,3,1,1,4]
    
    The minimum number of jumps to reach the last index is 2. (Jump 1 step from index 0 to 1, then 3 steps to the last index.)
*/
// -- SOLUTION
/*
    By christian on mitbbs
*/

public class Solution {
    public int jump(int[] A) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int count = 0;
        int start=0, end=0, newend;
        while (end < A.length-1){
            count++;
            newend = 0;
            for (int i=start; i<=end; i++)
                newend = i+A[i]>newend ? (i+A[i]) : newend;
            start = end+1;
            end = newend;
        }
        return count;
    }
}
