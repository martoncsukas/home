// -- http://oj.leetcode.com/problems/largest-rectangle-in-histogram/
// -- QUESTION
/*
    Given n non-negative integers representing the histogram's bar height where the width of each bar is 1, 
    find the area of largest rectangle in the histogram.
    For example,
    Given height = [2,1,5,6,2,3],
    return 10.
*/
// -- SOLUTION
/*
        judge small:passed
        judge larget: time limit exceeded in the last test case.
*/
    public class Solution {
        public int largestRectangleArea(int[] height) {
            // Start typing your Java solution below
            // DO NOT write main() function
            if(height.length==0)
                return 0;
                    
            int max = 0;
            for(int i=0;i<height.length;i++){
                int lowest = height[i];
                for(int j=i;j<height.length;j++){
                        if(height[j]<lowest)
                            lowest = height[j];
                        int area = lowest*(j+1-i);
                        if(area>max)
                            max =area;
                }
            }
                                
            return max;
        }
    }
