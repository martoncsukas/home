// -- http://oj.leetcode.com/problems/length-of-last-word/
// -- QUESTION
/*
    Given a string s consists of upper/lower-case alphabets and empty space characters ' ', 
    return the length of last word in the string.

    If the last word does not exist, return 0.
    
    Note: A word is defined as a character sequence consists of non-space characters only.
    
    For example, 
    Given s = "Hello World",
    return 5.
*/
// -- SOLUTION
/*
    if split() is a taboo, then...
*/

public class Solution {
    public int lengthOfLastWord(String s) {
        // Start typing your Java solution below
        // DO NOT write main() function
            int lastletter=-1,firstletter=0;
            for(int i=s.length()-1;i>=0;i--)
                if(s.charAt(i)!=' ') {
                    lastletter = i;
                    break;
                }
            
            for(int i=lastletter-1;i>=0;i--)
                if(s.charAt(i)==' '){
                    firstletter = i+1;
                    break;
                }
                    
            return lastletter-firstletter+1;
    }
}
