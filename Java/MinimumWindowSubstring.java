// -- http://oj.leetcode.com/problems/minimum-window-substring/
// -- QUESTION
/*
    Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).

    For example,
    S = "ADOBECODEBANC"
    T = "ABC"
    Minimum window is "BANC".
    
    Note:
    If there is no such window in S that covers all characters in T, return the emtpy string "".
    
    If there are multiple such windows, you are guaranteed that there will always be only one unique minimum window in S.
*/
// -- SOLUTION
/*
        O(n) time complexity solution
        Rewritten from http://www.leetcode.com/2010/11/finding-minimum-window-in-s-which.html.
*/

public class Solution {
    public String minWindow(String S, String T) {
            int SLen=S.length();
                int TLen=T.length();
                int[] needToFind = new int[256];
                
                for(int i=0;i<TLen;i++)
                    needToFind[T.charAt(i)]++;

                int[] hasFound = new int[256];
                int minWindowLen =Integer.MAX_VALUE;
                int count=0;
                int minWindowBegin=0;
                int minWindowEnd =0;
                for(int begin=0,end=0;end<SLen;end++){
                                char bchar = S.charAt(begin);
                                char echar = S.charAt(end);

                                if(needToFind[echar]==0)
                            continue;
                            
                                        hasFound[echar]++;       
                                        if(hasFound[echar]<=needToFind[echar])    
                                                count++;

                                        if(count == TLen){
                                                while(needToFind[bchar]==0|| hasFound[bchar] > needToFind[bchar]){
                                                        if(hasFound[bchar] > needToFind[bchar])
                                                                        hasFound[bchar]--;
                                                        bchar = S.charAt(++begin);
                                                        
                                                }

                                                int windowLen = end-begin+1;
                                                if(windowLen < minWindowLen){
                                                        minWindowBegin = begin;
                                                        minWindowEnd = end;
                                                        minWindowLen = windowLen;
                                                }
                                        } 
                }
                return count==TLen?S.substring(minWindowBegin,minWindowEnd+1):"";
                
            }
}
