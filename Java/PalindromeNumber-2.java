// -- http://oj.leetcode.com/problems/palindrome-number/#
// -- QUESTION
/*
    Determine whether an integer is a palindrome. Do this without extra space.
    Could negative integers be palindromes? (ie, -1)
    If you are thinking of converting the integer to string, note the restriction
    of using extra space.
    You could also try reversing an integer. However, if you have solved the problem 
    "Reverse Integer", you know that the reversed integer might overflow. 
    How would you handle such case?
    There is a more generic way of solving this problem.
*/

bool isNumberPalindrome2(int x, int &y) {
  if (x < 0) return false;
  if (x == 0) return true;
  if (isPalindrome(x/10, y) && (x%10 == y%10)) {
    y /= 10;
    return true;
  } else {
    return false;
  }
}
bool isPalindrome(int x) {
  return isPalindrome(x, x);
}
