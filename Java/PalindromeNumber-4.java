// -- http://oj.leetcode.com/problems/palindrome-number/#
// -- QUESTION
/*
    Determine whether an integer is a palindrome. Do this without extra space.
    Could negative integers be palindromes? (ie, -1)
    If you are thinking of converting the integer to string, note the restriction
    of using extra space.
    You could also try reversing an integer. However, if you have solved the problem 
    "Reverse Integer", you know that the reversed integer might overflow. 
    How would you handle such case?
    There is a more generic way of solving this problem.
*/

public boolean isNumberPalindrome4(int v) {
      if (v < 10) {
            return true;
      }
      int rem = v %10;
      int rev = 0, r = 0;
      int origVal = v/10;
      while (v > 10) {
          r = v % 10;
          rev = rev*10 + r;
          v = v / 10;
      }
      if (v == rem && rev == origVal) {
          return true;
      }
      return false;
}
