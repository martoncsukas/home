// -- http://oj.leetcode.com/problems/palindrome-number/
// -- QUESTION
/*
    Determine whether an integer is a palindrome. Do this without extra space.
    Could negative integers be palindromes? (ie, -1)
    If you are thinking of converting the integer to string, note the restriction of using extra space.
    You could also try reversing an integer. However, if you have solved the problem "Reverse Integer", 
    you know that the reversed integer might overflow. How would you handle such case?
    There is a more generic way of solving this problem.
*/

public class Solution {
    public boolean isPalindrome(int x) {
        // Start typing your Java solution below
        // DO NOT write main() function
        if(x<0) return false;
        if(x<10) return true;
        
        int divident = 10;
        while(x/divident>=10)
            divident*=10;
        
        int mod = 10;
        boolean yes = true;
        while(divident>=mod){
            yes&=x/divident==x%mod;
            x = x%divident/10;
            divident/=100;    
        }      
        return yes;
    }
}
