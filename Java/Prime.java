// -- http://leetcode.com/2010/04/finding-prime-numbers.html
// -- QUESTION
/*
    Output all prime numbers up to a specified integer n.
*/
// -- SOLUTION
/* 
    Generate a prime list from 0 up to n, using The Sieve of Erantosthenes
    param n The upper bound of the prime list (including n)
    param prime[] An array of truth value whether a number is prime
*/

public class Interpreter {
    public static void main(String[] args) {
        // Start typing your code here...
        System.out.println("Hello world!");
        getPrimeNumberSmallerThan(50000);
    }
    
    public static void getPrimeNumberSmallerThan(int n) {
        for (int i=2; i<= 3; i++) {
            System.out.println(i + " ");
        }
        for (int i = 3; 2 * i - 1 <= n; i=i+3) {
            for (int j = 0; j<=1; j++) {
                boolean isPrime = true;
                int temp = 2 * (i+j) -1;
                for (int t = 2; t*t <= temp; t++){
                    if (temp % t == 0) {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime) {
                    System.out.println(temp + " ");
                }
            }
        }
        
    }
}
