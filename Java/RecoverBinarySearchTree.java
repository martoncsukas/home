// -- http://oj.leetcode.com/problems/recover-binary-search-tree/
// -- QUESTION
/*
    Two elements of a binary search tree (BST) are swapped by mistake.
    Recover the tree without changing its structure.
    A solution using O(n) space is pretty straight forward. 
    Could you devise a constant space solution?
    confused what "{1,#,2,3}" means? > read more on how binary tree is serialized on OJ.
    
    OJ's Binary Tree Serialization:
    The serialization of a binary tree follows a level order traversal, where '#' signifies 
    a path terminator where no node exists below.
    
    Here's an example:
       1
      / \
     2   3
        /
       4
        \
         5
    The above binary tree is serialized as "{1,2,3,#,#,4,#,#,5}".
*/
// -- SOLUTION
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
 
public class Solution {
     TreeNode firstNode;
    TreeNode secondNode;
    public void recoverTree(TreeNode root) {
        // Start typing your Java solution below
        // DO NOT write main() function
        firstNode =null;
        secondNode = null;
        inorder(root,null);
        swap(firstNode,secondNode);


    }

    public void swap(TreeNode f,TreeNode s){
            int temp = f.val;
            f.val = s.val;
            s.val = temp;
    }

    public TreeNode inorder(TreeNode node, TreeNode prev){
            if(node==null) return prev;

            TreeNode p = inorder(node.left,prev);

            if(p!=null && p.val>node.val){
                    if(firstNode==null){
                            firstNode = p;
                            secondNode = node;
                    }
                    else
                            secondNode = node;
            }

            p = inorder(node.right,node);

            return p;
    }
}
