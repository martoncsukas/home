// -- http://oj.leetcode.com/problems/remove-duplicates-from-sorted-array/
// -- QUESTION
/*
    Given a sorted array, remove the duplicates in place such that each element appear only 
    once and return the new length.
    Do not allocate extra space for another array, you must do this in place with constant memory.
    For example, given input array A = [1,1,2],
    Your function should return length = 2, and A is now [1,2].
*/

public class Solution {
    public int removeDuplicates(int[] A) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int i=1;

        while(i<A.length){
            if(A[i]>A[i-1])
                i++;
            else{
                if(A[i-1]>=A[A.length-1])
                    return i;
                
                for(int j=i+1;j<A.length;j++)
                    if(A[j]>A[i-1]){
                        swap(A,i,j);
                        break;
                    }            
            }
        }   
        return A.length;
    }
    
    public void swap(int[] A,int i,int j){
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }
}
