// -- http://oj.leetcode.com/problems/remove-element/
// -- QUESTION
/*
    Given an array and a value, remove all instances of that value in place and return the new length.
    The order of elements can be changed. It doesn't matter what you leave beyond the new length.
*/

public class Solution {
    public int removeElement(int[] A, int elem) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int i=0, j=A.length-1;
        
        while(i<=j){
            if(A[i]==elem)
                swap(A,i,j--);
            else 
                i++;
        }
        return j+1;
    }
    
    public void swap(int[] A,int i, int j){
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }
}
