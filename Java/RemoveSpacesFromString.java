// -- http://leetcode.com/2010/02/c-code-to-remove-spaces-from-string.html
// -- QUESTION
/*
    Write a C function to remove spaces from a string. The function header should be 
    void removeSpaces(char *str)
    ie, “abc de” -> “abcde”
*/

public class removeSpacesFromString1 {
 
    @Test
    public void test() {
        removeSpaces("abc de".toCharArray());
        removeSpaces("abcde".toCharArray());
        removeSpaces(" abcde".toCharArray());
        removeSpaces("         abcde".toCharArray());
        removeSpaces("abcde ".toCharArray());
        removeSpaces("abcde     ".toCharArray());
        removeSpaces(" a b c d e ".toCharArray());
        removeSpaces("".toCharArray());
        removeSpaces("    ".toCharArray());
        removeSpaces(" ".toCharArray());
        removeSpaces(null);
        removeSpaces(" a ".toCharArray());
        removeSpaces("a ".toCharArray());
        removeSpaces(" a".toCharArray());
    }
 
    public char[] removeSpaces(char[] source) {
        if (source == null) {
            return null;
        }
        int oldIndex = 0;
        int newIndex = 0;
        while (oldIndex < source.length) {
            if (source[oldIndex] != ' ' && source[oldIndex] != '\t') {
                source[newIndex] = source[oldIndex];
                newIndex++;
            }
            oldIndex++;
        }
        if (oldIndex != newIndex) {
            source[newIndex] = '\0';
        }
        return Arrays.copyOf(source, newIndex);
    }
}
