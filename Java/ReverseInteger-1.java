// -- http://oj.leetcode.com/problems/reverse-integer
// -- QUESTION
/*
    Reverse digits of an integer.
    Example1: x = 123, return 321
    Example2: x = -123, return -321
    
    If the integer's last digit is 0, what should the output be? ie, cases such as 10, 100.
    Did you notice that the reversed integer might overflow? Assume the input is a 32-bit integer, 
    then the reverse of 1000000003 overflows. How should you handle such cases?
    Throw an exception? Good, but what if throwing an exception is not an option? You would then 
    have to re-design the function (ie, add an extra parameter).
*/

public class Solution {
    public int reverse(int x) {
        int sign = x>=0?1:-1;
        x = x>=0? x:-x;
        
        int res = 0;
        
        while(x!=0){
            res *= 10;
            res +=x%10;
            x = x/10;            
        }
            
        res = res*sign;
        
        return res;
    }
}
