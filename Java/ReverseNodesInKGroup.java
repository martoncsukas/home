// -- http://oj.leetcode.com/problems/reverse-nodes-in-k-group/
// -- QUESTION
/*
    Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.
    If the number of nodes is not a multiple of k then left-out nodes in the end should remain as it is.
    You may not alter the values in the nodes, only nodes itself may be changed.
    Only constant memory is allowed.
    For example, given this linked list: 1->2->3->4->5
    For k = 2, you should return: 2->1->4->3->5
    For k = 3, you should return: 3->2->1->4->5
*/
// -- SOLUTION
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */


public class Solution {
    public ListNode reverseKGroup(ListNode head, int k) {
        // Start typing your Java solution below
        // DO NOT write main() function
        
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode tail = dummy;
        ListNode kbegin = null;
        ListNode kend = null;

        ListNode node = head;
        int count =0;
        while(node!=null){
            if(count%k==0)
                        kbegin = node;
                if(count%k==k-1)
                        kend = node;
                node = node.next;
                if(count%k==k-1){
                        kend.next = null;
                        ListNode prev = null;
                        ListNode t = kbegin;
                        while(t!=null){
                                ListNode temp = t.next;
                                t.next = prev;
                                prev = t;
                                t = temp;
                        }
                        tail.next = kend;
                        tail = kbegin;
                        tail.next = node;
                }
                count++;


        }
        return dummy.next;
    }
}
