// -- http://oj.leetcode.com/problems/search-a-2d-matrix/
// -- QUESTION
/*
    Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:
    Integers in each row are sorted from left to right.
    The first integer of each row is greater than the last integer of the previous row.
    For example, consider the following matrix:
    [
      [1,   3,  5,  7],
      [10, 11, 16, 20],
      [23, 30, 34, 50]
    ]
    Given target = 3, return true.
*/
// -- SOLUTION
/*
        Note: In Arrays.binarySearch(array,target), if it can't find the target, it won't simply return -1.
        Another interesting solution:https://github.com/azheanda/leetcode-1/blob/master/search_a_2d_matrix.cpp
*/

import java.util.Arrays;
public class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int level = matrix.length-1;
        for(int i=0;i<matrix.length;i++){
                    if(target<matrix[i][0]){
                            level = i-1; 
                            break;
                    }
        }

            if(level>=0)
                    return Arrays.binarySearch(matrix[level],target)<0?false:true;
            else 
                    return false;
    }

}
