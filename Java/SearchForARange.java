// -- http://oj.leetcode.com/problems/search-for-a-range/
// -- QUESTION
/*
    Given a sorted array of integers, find the starting and ending position of a given target value.
    Your algorithm's runtime complexity must be in the order of O(log n).
    If the target is not found in the array, return [-1, -1].
    For example, given [5, 7, 7, 8, 8, 10] and target value 8, return [3, 4].
*/

public class Solution {
    public int[] searchRange(int[] A, int target) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int[] res = new int[2];
        res[0] = search(A, target-1)+1;
        res[1] = search(A, target);
        if(res[1] == -1 || A[res[1]] != target){
            res[0] = -1;
                res[1] = -1;
        }
        return res;

    }

    // return the biggest index of the element that is <= target
    // if all the elements in A is > x, return -1;
    public int search(int [] A, int target){
                 int start = 0, end = A.length-1, mid = end/2;
                 int res = -1;

                 while(start<=end){
                         if(A[mid]>target)
                                 end = mid-1;
                         else {
                                 start = mid+1;
                                 res = mid;
                         }
                         mid = (start+end)/2;
                 }
                 return res;

    }
}
