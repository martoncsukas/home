// -- http://leetcode.com/2010/04/searching-element-in-rotated-array.html
// -- QUESTION
/*
    Suppose a sorted array is rotated at some pivot unknown to you beforehand. 
    (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2). How do you find an element 
    in the rotated array efficiently? You may assume no duplicate exists in the array.
*/

public class Solution {
    public int search(int[] A, int target) {
        // Start typing your Java solution below
        // DO NOT write main() function
                 int start = 0;
                 int end = A.length-1;

                 while(start<=end){
                         int mid = (start+end)/2;
                         if(A[mid] == target) 
                return mid;
                         else if(A[start]<=A[mid]){  // check the first part is sorted or not
                                 if(A[mid]>target && A[start]<=target)
                                         end = mid-1;
                                 else
                                         start = mid+1;        
                         }else{                                                // check the second part
                                 if(A[mid]<target && A[end]>=target)
                                         start = mid+1;
                                 else
                                         end = mid-1;
                         }
                 }
                 return -1;
    }


}
