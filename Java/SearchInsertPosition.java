// -- http://oj.leetcode.com/problems/search-insert-position/
// -- QUESTION
/*
    Given a sorted array and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
    You may assume no duplicates in the array.
    Here are few examples.
    [1,3,5,6], 5 → 2
    [1,3,5,6], 2 → 1
    [1,3,5,6], 7 → 4
    [1,3,5,6], 0 → 0
*/

public class Solution {
    public int searchInsert(int[] A, int target) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int lo = 0, hi = A.length-1, mid = hi/2;

        while(lo<=hi){
            mid = (lo+hi)/2;
            if(target == A[mid])
                return mid;
            else if(target > A[mid])
                lo = mid+1;
            else
                hi = mid-1;         
        }
        return lo;

    }
}

// This method is unnecessarily complicated

public class Solution {
    public int searchInsert(int[] A, int target) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int lo = 0, hi = A.length-1, mid = hi/2;

        while(lo<=hi){
            mid = (lo+hi)/2;
            if(target == A[mid])
                        return mid;
                else if(target > A[mid]){
                        if(lo==hi)
                                return mid+1;
                        lo = mid+1;
                }
                else{
                        if(lo==hi)
                                return mid;
                        hi = mid-1;
                }
        }
        return mid;

    }
}
