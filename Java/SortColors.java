// -- http://oj.leetcode.com/problems/sort-colors/
// -- QUESTION
/*
    Given an array with n objects colored red, white or blue, sort them so that objects 
    of the same color are adjacent, with the colors in the order red, white and blue.
    Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.
    Note: You are not suppose to use the library's sort function for this problem.
    A rather straight forward solution is a two-pass algorithm using counting sort.
    First, iterate the array counting number of 0's, 1's, and 2's, then overwrite array with 
    total number of 0's, then 1's and followed by 2's.
    Could you come up with an one-pass algorithm using only constant space?
*/
/*
    3-way partition
    loop invariant:
        A[0->p]==0
        A[p->r]== unknown
        A[r->A.length-1]==2 
*/

public class Solution {
    public void sortColors(int[] A) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int p=-1,r=A.length;
        for(int q=0;q<r;q++){
            if(A[q]==0){
                p++;
                swap(A,p,q);
            }else if(A[q]==2){
                r--;
                swap(A,r,q);
                q--;   // This is because when q hits a 2, the A[r] I swap it with is an unknown value. So I let "q--",then next loop will check that value.
            }
        }       
    }
    
    public void swap(int[] A,int i,int j){
        int temp = A[i];
        A[i]=A[j];
        A[j] = temp;
    }
}
