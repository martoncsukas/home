// -- http://leetcode.com/2010/05/printing-matrix-in-spiral-order.html
// -- QUESTION
/*
    Given a matrix (2D array) of m x n elements (m rows, n columns), write a function that 
    prints the elements in the array in a spiral manner.
*/
// -- SOLUTION 
/* 
    It’s the same with printing the matrix spirally! Print the outer layer of the matrix, 
    then print the remaining sub-matrix recursively. Please do not forget to handle the base case! 
    (Hint: Check when m == 1 or n == 1).
*/
/*
    It took a lot of time for me to figure out about the base case and how to add it into the program. 
    It would be easier to write a recursive verison:http://www.leetcode.com/2010/05/printing-matrix-in-spiral-order.html
    A little revision of spiralmatrix.java
*/

public class Solution {
    public int[][] generateMatrix(int n) {
        // Start typing your Java solution below
        // DO NOT write main() function
        int[][] res = new int[n][n];
        if(n==0)
            return res;
        
        int lvl = (n+1)/2;
        int c=1;
        
        
        for(int clvl=0;clvl<lvl;clvl++){          
            for(int j=clvl;j<n-clvl;j++)        // top
                res[clvl][j]=c++;
            for(int i=clvl+1;i<n-clvl;i++)      // right
                res[i][n-clvl-1]=c++;     
            if(clvl < n-clvl-1)
                for(int j=n-clvl-2;j>=clvl;j--)     // bot
                    res[n-clvl-1][j]=c++;
            if(clvl < n-clvl-1)
                for(int i=n-clvl-2;i>clvl;i--)      // left
                    res[i][clvl]=c++;              
        }
                    
        
        return res;
        
        
        
        
    }
}
