// -- http://oj.leetcode.com/problems/swap-nodes-in-pairs/
// -- QUESTION
/*
    Given a linked list, swap every two adjacent nodes and return its head.
    For example, given 1->2->3->4, you should return the list as 2->1->4->3.
    Your algorithm should use only constant space. You may not modify the values in the list, 
    only nodes itself can be changed.
*/
// -- SOLUTION
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
 
public class Solution {
    public ListNode swapPairs(ListNode head) {
        // Start typing your Java solution below
        // DO NOT write main() function
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode curr = dummy;
        ListNode node1 = null;
        ListNode node2 = null;
        
        while(curr.next!=null && curr.next.next!=null){
            node1 = curr.next;            
            node2 = node1.next;
            ListNode next =node2.next;            
            curr.next = node2;
            node2.next = node1;
            node1.next = next;            
            curr=node1;
        }
        return dummy.next;
    }
}
