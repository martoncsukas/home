//---------------------------------------------------------------------------------------------------------------
// -- http://www.careercup.com/question?id=14955548
//---------------------------------------------------------------------------------------------------------------
// -- QUESTION
/*

	There is a toycar placed on a 5 by 5 board.We can give 5 commands to it:
	PLACE(X,Y,F) where x denotes X Axis,y denotes Y Axis and F denotes the direction to which its facing.
	MOVE->Move will move the toycar one step in the direction where its facing
	LEFT->Left will turn the toycar by 90 degrees to its left and face it to the new direction. 
	   Note:Left will not move the toycar, it will just change the direction
	RIGHT->Right will turn the toycar by 90 degrees to its right and face it to the new direction. 
	   Note:Right will not move the toycar, it will just change the direction
	REPORT->Report shall tell me the X Axis,YAxis and Direction of the toycar. like 0,0,NORTH
	   Note:You cannot move,left,right,report the toycar unless you place it.
*/
//---------------------------------------------------------------------------------------------------------------

package toycar;

import java.util.HashSet;
import java.util.Set;

public class ToycarGame {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Board board = new Board(5, 5);
		ToyCar car = new ToyCar();
		try {
			car.setSurface(board, 0, 0, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(car.report());
		car.moveForward();
		System.out.println(car.report());
		car.turnRight();
		car.moveForward();
		System.out.println(car.report());
		car.turnRight();
		car.moveForward();
		System.out.println(car.report());
		car.turnRight();
		car.moveForward();
		System.out.println(car.report());
		car.turnRight();
		car.moveForward();
		System.out.println(car.report());		
	}

	public static class Board
	{
		Set <ToyCar> cars = new HashSet();
		int w, h;
		
		public Board(int w, int h)
		{
			this.w = w;
			this.h = h;
		}
		
		public boolean checkConstraints(int x, int y) 
		{
			if(x < 0 || x >= w || y < 0 || y >= h)
				return false;
			else
				return true;
		}
		
		
	}
	
	public static class ToyCar
	{
		int x, y;
		//0 = up, 1 = right, 2 = down, 3 = left
		int facing = 0; 
		Board surface;
		boolean placed = false;
		
		public ToyCar()
		{
		}
		
		public void setSurface(Board board, int x, int y, int facing) throws Exception
		{
			this.x = x;
			this.y = y;
			this.facing = facing;
			this.surface = board;
			if(!surface.checkConstraints(x, y))
				throw new Exception("Invalid Position");
			
			if(surface != null)
				placed = true;
			else
				placed = false;
		}

		public boolean moveForward()
		{
			switch(facing)
			{
			case 0:
				return moveTo(x, y+1);
			case 1:
				return moveTo(x+1, y);
			case 2: 
				return moveTo(x, y-1);
			case 3: 
				return moveTo(x-1, y);					
			}
			return false;
		}

		private boolean moveTo(int x, int y)
		{
			if(surface.checkConstraints(x,y))
			{
				this.x = x; 
				this.y = y;
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public void turnLeft()
		{
			if(facing == 0)
				facing = 3;
			else
				facing--;
		}
		
		public void turnRight()
		{
			facing = (facing+1)%4;
		}
		
//		private void setFacing(int facing) {
//			this.facing = facing;
//		}

		public String report()
		{
			String dir = "";
			switch(facing)
			{
			case 0:
				dir = "NORTH";
				break;
			case 1:
				dir = "WEST";
				break;
			case 2: 
				dir = "SOUTH";
				break;
			case 3: 
				dir = "EAST";
				break;
			}
			return x + ", " + y + ", " + dir;
		}
		
		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public int getFacing() {
			return facing;
		}
		
	}
	
}
