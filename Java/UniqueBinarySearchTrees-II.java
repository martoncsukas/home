// -- http://oj.leetcode.com/problems/unique-binary-search-trees-ii/
// -- QUESTION
/*
    Given n, generate all structurally unique BST's (binary search trees) that store values 1...n.
For example, given n = 3, your program should return all 5 unique BST's shown below.

   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3
*/
// -- SOLUTION
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; left = null; right = null; }
 * }
 */
 
public class Solution {
    public ArrayList<TreeNode> generateTrees(int n) {
        // Start typing your Java solution below
        // DO NOT write main() function
        ArrayList<TreeNode> res = new ArrayList<TreeNode>();
        generateTrees(res,1,n);
        return res;
                        
    }

    public void generateTrees(ArrayList<TreeNode> res,int left,int right){
            if(left > right)
                    res.add(null);
            else
                    for(int i=left;i<=right;i++){
                            ArrayList<TreeNode> lefts = new ArrayList<TreeNode>();
                            generateTrees(lefts,left,i-1);
                            ArrayList<TreeNode> rights = new ArrayList<TreeNode>();
                            generateTrees(rights,i+1,right);
                            for(int x=0;x<lefts.size();x++)
                                    for(int y=0;y<rights.size();y++){
                                            TreeNode root = new TreeNode(i);
                                            root.left = lefts.get(x);
                                            root.right = rights.get(y);
                                            res.add(root);
                                    }
                    }
    }
}
